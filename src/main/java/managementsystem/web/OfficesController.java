package managementsystem.web;
import managementsystem.domain.Offices;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/officeses")
@Controller
@RooWebScaffold(path = "officeses", formBackingObject = Offices.class)
public class OfficesController {
}
