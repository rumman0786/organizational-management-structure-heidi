package managementsystem.web;
import managementsystem.domain.Worker_dept_join;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/worker_dept_joins")
@Controller
@RooWebScaffold(path = "worker_dept_joins", formBackingObject = Worker_dept_join.class)
public class Worker_dept_joinController {
}
