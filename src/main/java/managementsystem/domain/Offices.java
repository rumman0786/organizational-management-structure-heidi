package managementsystem.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Offices {

    /**
     */
    private String office_name;

    /**
     */
    @ManyToOne
    private Company belongs_to_company;

    /**
     */
    @ManyToOne
    private City belongs_to_city;
}
