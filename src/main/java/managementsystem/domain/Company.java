package managementsystem.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Version;

@Entity
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Company {

    /**
     */
    private String company_name;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Offices> offices = new HashSet<Offices>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Department> departments = new HashSet<Department>();
    
    public static List<Company> companyList() {
        return entityManager().createQuery("select o from Comany o ").getResultList();
    }

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

	@Version
    @Column(name = "version")
    private Integer version;

	public Long getId() {
        return this.id;
    }

	public void setId(Long id) {
        this.id = id;
    }

	public Integer getVersion() {
        return this.version;
    }

	public void setVersion(Integer version) {
        this.version = version;
    }
}
